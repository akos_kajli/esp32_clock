/*
 * gui.c
 *
 *  Created on: 2021. szept. 5.
 *      Author: Bob
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_freertos_hooks.h"
#include "freertos/semphr.h"
#include "esp_system.h"
#include "esp_log.h"
#include "driver/gpio.h"

#include "lvgl.h"
#include "lvgl_helpers.h"

#include "main.h"
#include "gui.h"
#include "startScreen.h"
#include "menuScreen.h"

static void lv_tick_task(void *arg);
#define LV_TICK_PERIOD_MS 1

void guiTask(void *pvParameter) {

    (void) pvParameter;

    lv_init();

    /* Initialize SPI or I2C bus used by the drivers */
    lvgl_driver_init();

    lv_color_t* buf1 = heap_caps_malloc(DISP_BUF_SIZE * sizeof(lv_color_t), MALLOC_CAP_DMA);
    assert(buf1 != NULL);
    lv_color_t* buf2 = heap_caps_malloc(DISP_BUF_SIZE * sizeof(lv_color_t), MALLOC_CAP_DMA);
    assert(buf2 != NULL);

    static lv_disp_buf_t disp_buf;

    uint32_t size_in_px = DISP_BUF_SIZE;

    /* Initialize the working buffer depending on the selected display.
     * NOTE: buf2 == NULL when using monochrome displays. */
    lv_disp_buf_init(&disp_buf, buf1, buf2, size_in_px);

    lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.flush_cb = disp_driver_flush;

    disp_drv.buffer = &disp_buf;
    lv_disp_drv_register(&disp_drv);

#if CONFIG_LV_TOUCH_CONTROLLER != TOUCH_CONTROLLER_NONE
    lv_indev_drv_t indev_drv;
    lv_indev_drv_init(&indev_drv);
    indev_drv.read_cb = touch_driver_read;
    indev_drv.type = LV_INDEV_TYPE_POINTER;
    lv_indev_drv_register(&indev_drv);
#endif

    /* Create and start a periodic timer interrupt to call lv_tick_inc */
    const esp_timer_create_args_t periodic_timer_args = {
        .callback = &lv_tick_task,
        .name = "periodic_gui"
    };
    esp_timer_handle_t periodic_timer;
    ESP_ERROR_CHECK(esp_timer_create(&periodic_timer_args, &periodic_timer));
    ESP_ERROR_CHECK(esp_timer_start_periodic(periodic_timer, LV_TICK_PERIOD_MS * 1000));

    dayText = "Day";
    monthText = "month";
    dayNr = 0;
    yearNr = 0;

    //create screen
    createStartScreen();
    activeScreen = startScreen;
    lv_scr_load(activeScreen);

    lv_obj_t* label=lv_label_create(startScreen, NULL);

    while (1) {
        /* Delay 1 tick (assumes FreeRTOS tick is 10ms */
        vTaskDelay(pdMS_TO_TICKS(5));
        if(!timeInit){
        	lv_label_set_text(label, "Szinkronizáció folyamatban...");
        	lv_obj_set_style_local_text_font(label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, &lora28);
        	lv_obj_align(label, startScreen, LV_ALIGN_CENTER, 0, 0);
        	lv_obj_set_hidden(dateLabel, 1);
        	lv_obj_set_hidden(timeLabel, 1);
        	lv_obj_set_hidden(menuButton, 1);
        }else{
        	lv_obj_set_hidden(label, 1);
        	lv_obj_set_hidden(dateLabel, 0);
        	lv_obj_set_hidden(timeLabel, 0);
        	lv_obj_set_hidden(menuButton, 0);
        }
        if(activeScreen == startScreen){
        	createDateLabel();
        	lv_label_set_text_fmt(timeLabel, "%s", stringArray[2]);
        }else if(activeScreen == menuScreen){
        	lv_label_set_text_fmt(menuTimeLabel, "%s", stringArray[2]);
        }

        /* Try to take the semaphore, call lvgl related function on success */
        if (pdTRUE == xSemaphoreTake(xGuiSemaphore, portMAX_DELAY)) {
            lv_task_handler();
            xSemaphoreGive(xGuiSemaphore);
       }
    }

    /* A task should NEVER return */
    free(buf1);
#ifndef CONFIG_LV_TFT_DISPLAY_MONOCHROME
    free(buf2);
#endif
    vTaskDelete(NULL);
}

static void lv_tick_task(void *arg) {
    (void) arg;

    lv_tick_inc(LV_TICK_PERIOD_MS);
}
