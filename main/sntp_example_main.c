/*
 * SNTP example by Espressif reworked to continuously give time values every second
 * TODO weather: https://api.openweathermap.org/data/2.5/weather?id=3046526&appid=169aabea8dbe560305e601a742ce6638
 * TODO: Fix wifi things, add calendar and note functionality-->nvs?
 *
 */
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_attr.h"
#include "esp_sleep.h"
#include "nvs_flash.h"
#include "protocol_examples_common.h"
#include "esp_sntp.h"

#include "main.h"
#include "gui.h"

static const char *TAG = "Clock";

static void obtainTime(void);
static void initializeSNTP(void);

time_t now;
struct tm timeinfo;

void timeSyncNotification(struct timeval *tv)
{
    ESP_LOGI(TAG, "Notification of a time synchronization event");
}

/*
 * String Cut function for parsing the time string
 */
void stringCut(char* buffer){
	int i;
	char* token="";
	const char s[2] = " ";
	i=0;
	strtok(buffer, s);
	while(token!=NULL){
		if(i>=1){
			strcpy(stringArray[i-1], token);
		}
		token=strtok(NULL, s);
		i++;
	}
}

void timeTask(void* pvParam){
	while(1){
	    time(&now);
	    localtime_r(&now, &timeinfo);
	    if (timeinfo.tm_year < (2021 - 1900)) {
	        ESP_LOGI(TAG, "Time is not set yet. Connecting to WiFi and getting time over NTP.");
	        obtainTime();
	        // update 'now' variable with current time
	        time(&now);
	    	timeInit=true;
	    }
	    // Set timezone to CEST and print local time
	    setenv("TZ", "UTC-2", 1);
	    tzset();
	    localtime_r(&now, &timeinfo);
	    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
	    stringCut(strftime_buf);
	    //Format: Day	Month	Day[nr]	Time(h:m:s)	Year-->Ex: Sat Sep 4 21:19:13 2021
	    //printf("%s\t%s\t%s\t%s\t%s\n", strftime_buf, stringArray[0], stringArray[1], stringArray[2], stringArray[3]);
		vTaskDelay(100);
	}
}

void app_main(void)
{
	timeInit = false;

	xGuiSemaphore = xSemaphoreCreateMutex();

	memset(stringArray, 0, sizeof(stringArray));

	//two tasks on different cores to reduce delay
    xTaskCreatePinnedToCore(guiTask, "gui", 4096*2, NULL, 0, NULL, 1);

    ESP_ERROR_CHECK( nvs_flash_init() );
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    ESP_ERROR_CHECK(example_connect());

    xTaskCreatePinnedToCore(timeTask, "time", 4096*2, NULL, 0, NULL, 0);
}

static void obtainTime(void)
{
    initializeSNTP();

    // wait for time to be set
    time_t now = 0;
    struct tm timeinfo = { 0 };
    int retry = 0;
    const int retry_count = 10;
    while (sntp_get_sync_status() == SNTP_SYNC_STATUS_RESET && ++retry < retry_count) {
        ESP_LOGI(TAG, "Waiting for system time to be set... (%d/%d)", retry, retry_count);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
    time(&now);
    localtime_r(&now, &timeinfo);
}

static void initializeSNTP(void)
{
    ESP_LOGI(TAG, "Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    sntp_set_time_sync_notification_cb(timeSyncNotification);
    sntp_init();
}
