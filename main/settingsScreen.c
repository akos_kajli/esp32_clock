/*
 * settingsScreen.c
 *
 *  Created on: 2021. szept. 12.
 *      Author: Bob
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lvgl.h"
#include "lvgl_helpers.h"

#include "EVE.h"
#include "EVE_commands.h"

#include "main.h"
#include "gui.h"
#include "startScreen.h"
#include "menuScreen.h"
#include "settingsScreen.h"

lv_obj_t* fromtmp;
static lv_obj_t * lightSliderLabel;

static void backEvent(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED) {
    	lv_obj_del(settingsScreen);
    	lv_scr_load(fromtmp);
    	activeScreen = fromtmp;
    }
}

static void lightSliderEvent(lv_obj_t * slider, lv_event_t event)
{
    if(event == LV_EVENT_VALUE_CHANGED) {
        static char buf[4];
        snprintf(buf, 4, "%u", lv_slider_get_value(slider));
        lv_label_set_text(lightSliderLabel, buf);
        EVE_memWrite8(REG_PWM_DUTY, lv_slider_get_value(slider)+28);
    }
}

void createSettingsScreen(lv_obj_t* from){
	fromtmp=from;

	settingsScreen=lv_obj_create(NULL, NULL);

	lv_obj_t* backButton = lv_btn_create(settingsScreen, NULL);
	lv_obj_t* label = lv_label_create(backButton, NULL);
	lv_label_set_text(label, LV_SYMBOL_HOME);
	lv_obj_align(backButton, settingsScreen, LV_ALIGN_IN_BOTTOM_RIGHT, -10, -10);
	lv_obj_set_event_cb(backButton, backEvent);

    lv_obj_t * lightSlider = lv_slider_create(settingsScreen, NULL);
    lv_obj_set_width(lightSlider, 260);
    lv_obj_align(lightSlider, NULL, LV_ALIGN_CENTER, 0, 0);
    lv_slider_set_value(lightSlider, 0, LV_ANIM_OFF);
    lv_obj_set_event_cb(lightSlider, lightSliderEvent);
    lv_slider_set_range(lightSlider, 0, 100);

    /* Create a label below the slider */
    lightSliderLabel = lv_label_create(settingsScreen, NULL);
    lv_label_set_text(lightSliderLabel, "0");
    lv_obj_set_auto_realign(lightSliderLabel, true);
    lv_obj_align(lightSliderLabel, lightSlider, LV_ALIGN_OUT_BOTTOM_MID, 0, 10);
    lv_obj_set_style_local_text_font(lightSliderLabel, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, &lora28);

    /* Create an informative label */
    lv_obj_t * info = lv_label_create(settingsScreen, NULL);
    lv_label_set_text(info, "Fényerősség beállítása");
    lv_obj_align(info, lightSlider, LV_ALIGN_OUT_TOP_LEFT, 0, -50);
    lv_obj_set_style_local_text_font(info, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, &lora28);
}
