/*
 * menuScreen.c
 *
 *  Created on: 2021. szept. 12.
 *      Author: Bob
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lvgl.h"
#include "lvgl_helpers.h"

#include "main.h"
#include "gui.h"
#include "startScreen.h"
#include "menuScreen.h"
#include "settingsScreen.h"

lv_obj_t* fromtmp;

static void menuMatrixEvent(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_VALUE_CHANGED) {
        const char * txt = lv_btnmatrix_get_active_btn_text(obj);
        if(strstr(txt, "Beáll")){
        	createSettingsScreen(menuScreen);
        	lv_scr_load(settingsScreen);
        	activeScreen=settingsScreen;
        }else if(strstr(txt, "Nap")){

        }
    }
}

static void backEvent(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED) {
    	lv_obj_del(menuScreen);
    	lv_scr_load(startScreen);
    	activeScreen = startScreen;
    }
}

static const char * btnm_map[] = {"Beállítások", "Naptár",""};

void createMenuScreen(lv_obj_t* from){
	fromtmp=from;

	menuScreen = lv_obj_create(NULL, NULL);

	lv_obj_t* backButton = lv_btn_create(menuScreen, NULL);
	lv_obj_t* label = lv_label_create(backButton, NULL);
	lv_label_set_text(label, LV_SYMBOL_HOME);
	lv_obj_align(backButton, menuScreen, LV_ALIGN_IN_BOTTOM_RIGHT, -10, -10);
	lv_obj_set_event_cb(backButton, backEvent);

    lv_obj_t * btnm1 = lv_btnmatrix_create(menuScreen, NULL);
    lv_btnmatrix_set_map(btnm1, btnm_map);
    lv_obj_set_size(btnm1, 460, 200);
    lv_obj_align(btnm1, menuScreen, LV_ALIGN_IN_TOP_LEFT, 10, 10);
	lv_obj_set_style_local_text_font(btnm1, LV_BTNMATRIX_PART_BTN, LV_STATE_DEFAULT, &lora28);
    lv_obj_set_event_cb(btnm1, menuMatrixEvent);

    menuTimeLabel = lv_label_create(menuScreen, NULL);
    lv_obj_align(menuTimeLabel, menuScreen, LV_ALIGN_IN_BOTTOM_LEFT, 110, -35);
	lv_obj_set_style_local_text_font(menuTimeLabel, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, &lora36);

}
