/*
 * weatherAPI.c
 *
 *  Created on: 2021. okt. 3.
 *      Author: Bob
 */

#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "protocol_examples_common.h"
#include "esp_tls.h"
#include "cJSON.h"

#include "esp_http_client.h"
#include "weatherAPI.h"
