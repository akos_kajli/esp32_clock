/*
 * startScreen.c
 *
 *  Created on: 2021. szept. 5.
 *      Author: Bob
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lvgl.h"
#include "lvgl_helpers.h"

#include "main.h"
#include "gui.h"
#include "startScreen.h"
#include "menuScreen.h"

static void setDayText(){
	if(strstr(strftime_buf, "Sun")){
		dayText = "Vasárnap";
	}else if(strstr(strftime_buf, "Mon")){
		dayText = "Hétfő";
	}else if(strstr(strftime_buf, "Tue")){
		dayText = "Kedd";
	}else if(strstr(strftime_buf, "Wed")){
		dayText = "Szerda";
	}else if(strstr(strftime_buf, "Thu")){
		dayText = "Csütörtök";
	}else if(strstr(strftime_buf, "Fri")){
		dayText = "Péntek";
	}else if(strstr(strftime_buf, "Sat")){
		dayText = "Szombats";
	}
}

static void setMonthText(){
	if(strstr(stringArray[0], "Jan")){
		monthText = "Január";
	}else if(strstr(stringArray[0], "Feb")){
		monthText = "Február";
	}else if(strstr(stringArray[0], "Mar")){
		monthText = "Március";
	}else if(strstr(stringArray[0], "Apr")){
		monthText = "Április";
	}else if(strstr(stringArray[0], "May")){
		monthText = "Május";
	}else if(strstr(stringArray[0], "Jun")){
		monthText = "Június";
	}else if(strstr(stringArray[0], "Jul")){
		monthText = "Július";
	}else if(strstr(stringArray[0], "Aug")){
		monthText = "Augusztus";
	}else if(strstr(stringArray[0], "Sep")){
		monthText = "Szeptember";
	}else if(strstr(stringArray[0], "Oct")){
		monthText = "Október";
	}else if(strstr(stringArray[0], "Nov")){
		monthText = "November";
	}else if(strstr(stringArray[0], "Dec")){
		monthText = "December";
	}
}

static void menuEvent(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED) {
    	createMenuScreen(startScreen);
    	lv_scr_load(menuScreen);
    	activeScreen = menuScreen;
    }
}

void createDateLabel(){
	setDayText();
	setMonthText();
	dayNr = atoi(stringArray[1]);
	yearNr = atoi(stringArray[3]);
	lv_label_set_text_fmt(dateLabel, "%s\t%s\t%d\t%d", dayText, monthText, dayNr, yearNr);
}

void createStartScreen(void){
	startScreen = lv_obj_create(NULL, NULL);

	dateLabel = lv_label_create(startScreen, NULL);
	lv_obj_align(dateLabel, startScreen, LV_ALIGN_IN_TOP_LEFT, 50, 30);
	timeLabel = lv_label_create(startScreen, NULL);
	lv_obj_align(timeLabel, startScreen, LV_ALIGN_CENTER, -90, -30);

	menuButton = lv_btn_create(startScreen, NULL);
	lv_obj_t* label=lv_label_create(menuButton, NULL);
	lv_label_set_text(label, LV_SYMBOL_LIST);
	lv_obj_align(menuButton, startScreen, LV_ALIGN_IN_BOTTOM_LEFT, 10, -10);
	lv_obj_set_event_cb(menuButton, menuEvent);


	lv_obj_set_style_local_text_font(dateLabel, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, &lora28);
	lv_obj_set_style_local_text_font(timeLabel, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, &lora56);

}
