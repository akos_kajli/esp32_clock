/*
 * menuScreen.h
 *
 *  Created on: 2021. szept. 12.
 *      Author: Bob
 */

#ifndef MAIN_MENUSCREEN_H_
#define MAIN_MENUSCREEN_H_

lv_obj_t* menuScreen;
lv_obj_t* menuTimeLabel;

void createMenuScreen(lv_obj_t* from);

#endif /* MAIN_MENUSCREEN_H_ */
