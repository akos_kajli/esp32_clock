/*
 * settingsScreen.h
 *
 *  Created on: 2021. szept. 12.
 *      Author: Bob
 */

#ifndef MAIN_SETTINGSSCREEN_H_
#define MAIN_SETTINGSSCREEN_H_

lv_obj_t* settingsScreen;

void createSettingsScreen(lv_obj_t* from);

#endif /* MAIN_SETTINGSSCREEN_H_ */
