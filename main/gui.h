/*
 * gui.h
 *
 *  Created on: 2021. szept. 5.
 *      Author: Bob
 */

#ifndef MAIN_GUI_H_
#define MAIN_GUI_H_

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_freertos_hooks.h"
#include "freertos/semphr.h"
#include "esp_system.h"
#include "driver/gpio.h"

#include "lvgl.h"
#include "lvgl_helpers.h"

LV_FONT_DECLARE(lora28);
LV_FONT_DECLARE(lora56);
LV_FONT_DECLARE(lora36);

SemaphoreHandle_t xGuiSemaphore;

lv_obj_t* activeScreen;

void guiTask(void *pvParameter);

#endif /* MAIN_GUI_H_ */
