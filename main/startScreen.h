/*
 * startScreen.h
 *
 *  Created on: 2021. szept. 5.
 *      Author: Bob
 */

#ifndef MAIN_STARTSCREEN_H_
#define MAIN_STARTSCREEN_H_

lv_obj_t* startScreen;

char* dayText;
char* monthText;
uint8_t dayNr;
uint16_t yearNr;

lv_obj_t* dateLabel;
lv_obj_t* timeLabel;
lv_obj_t* menuButton;

void createDateLabel();

void createStartScreen(void);

#endif /* MAIN_STARTSCREEN_H_ */
