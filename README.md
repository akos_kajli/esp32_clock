# Project idea

Making use of an ESP32, an FT810 development board and a TFT touchscreen I have lying around I'm making a simple clock that displays the time and date.
Time comes from an SNTP server, graphical part makes use of LVGL libraries.